const db = require("../models")
const Post = db.posts

exports.findAll =  (req, res) => {
    Post.findAll({
      attributes: ['id','title','content','image','category','createdAt'],
      order:[ ['createdAt', 'DESC']]
    })
      .then(result => {
        res.json(result)
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving posts."
        })
   })
  }

exports.findOne =  (req, res) => {
 const id = req.params.id
 Post.findByPk(id)
    .then(result => {
      res.send(result)
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Post with id=" + id
      })
    })
}

exports.create = (req, res) => {
  const post = {
    title: req.body.title,
    content: req.body.content,
    image: req.body.image,
    category: req.body.category
    }
  Post.create(post)
   .then(result => {
      res.json(result)
    })
    .catch(err => {
      res.status(500).send({
         message:
           err.message || "Some error occurred while creating the Post."
      })
    })
}
exports.update = (req, res) => {
  const id = req.params.id;
  Post.update(req.body, {
    where: { id: id }
  })
   .then(num => {
      if (num == 1) {
        res.json({
          message: "Post was updated successfully."
        });
      } 
      else {
        res.json({
         message: `Cannot update Post with id=${id}!`
        });
      }
   })
   .catch(err => {
      res.status(500).send({
         message: "Error updating Post with id=" + id
      })
  })
}

exports.deleteOne =  (req, res) => {
  const id = req.params.id;
  Post.destroy({
    where: { id: id }
  })
  .then(num => {
     if (num == 1) {
      res.json({
        message: "Post was deleted successfully!"
      })
      }
      else {
        res.json({
          message: `Cannot delete Post with id=${id}. Maybe Post was not found!`
        })
      }
  })
  .catch(err => {
    res.status(500).send({
      message: "Could not delete Post with id=" + id
    })
  })
}
