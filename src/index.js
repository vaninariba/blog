const express = require('express')
const app = express()
const db = require("./models")

db.sequelize.sync({ force: true })
.then(() => {
  console.log("Sync database")
})

app.set('port', 3000)

app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use('/api/posts',require('./routes/blog.routes'))

app.listen(app.get('port'))
console.log('Server on port', app.get('port'))