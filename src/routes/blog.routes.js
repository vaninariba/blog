const express = require('express')
const router =express.Router()
const { findAll, findOne, create, update, deleteOne } = require('../controller/index.controller')

router.get('/', findAll)
router.get('/id', findOne)
router.post('/', create)
router.patch('/:id', update)
router.delete('/:id', deleteOne)

module.exports = router

